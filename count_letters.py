#ALPHABETS = "abcdefghijklmnopqrstuvwxyz"
def read_file(data: str) -> str:
    with open(data, "r") as file:
        allText = file.read()
        words = list(map(str, allText.split()))
    return random.choice(words)
def check_string(data1: list[str]) -> list[str]:
    letter = set()
    for item in data1:
        letter.update(item)
    return list(letter)
def similar_letters(guess: str, word: str) -> int:
    count = 0
    if guess == word:
        return -1
    else:
        for ch in guess:
            for i in word:
                if ch == i:
                    count += 1
    return count
def correct_guess(count: int) -> str:
    if count == -1:
        return f'Correct guess'
    else:
        return f'Not a correct guess'
word_from_file = read_file('5letters.txt')
print(correct_guess(similar_letters("yaptr", word_from_file)))                                                                          
