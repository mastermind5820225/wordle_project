import requests as rq
import json



session = rq.Session()
mm_url = "https://we6.talentsprint.com/wordle/game/"
register_url = mm_url + "register"
register_dict = {"mode" : "mastermind", "name": "Yashaswini"}
register_with = json.dumps(register_dict)
r = session.post(register_url, json=register_dict)
me = r.json()['id']
create_url = mm_url + "create"
create_dict = {"id": me, "overwrite": False}
rc = session.post(create_url, json=create_dict)
guess_url = mm_url + "guess"

def convert_str_to_set(word: str) -> set:
    letters = set()
    for char in word:
        letters.add(char)
    return letters

confirmed_letter = set()


def valid_words_data_arrange(file_name: str) -> list[str]:
    file = open(file_name, "r")
    return file.read().split()


def classifying_based_on_feedback(data: list[str]):
    for word in data:
        guess_dict = {"guess": word, "id": me}
        correct = session.post(guess_url, json=guess_dict)
        print(correct.json()['feedback'])
        if correct.json()['feedback'] == 5:
            print("Done 5!")
            break
        
def classify(data: list[str]) -> dict:
    guess_dict = {"guess": data[0], "id": me}
    index = 0
    number_of_valid_letters = {0: [], 1: [], 2: [], 3: [], 4: []}
    correct = session.post(guess_url, json=guess_dict)
    while index < len(data):
        curr_word = data[index]
        number_of_valid_letters[correct.json()['feedback']].append(curr_word)
        index += 1
        guess_dict = {"guess": curr_word, "id": me}
    return dealing_with_ones(number_of_valid_letters[1])

def dealing_with_ones(data: list[str]) -> str:
    if len(data) == 0:
        return 0
    index = 0
    intersected_letter = convert_str_to_set(data[index])
    while index < len(data) and len(intersected_letter) > 1:
        intersected_letter &= convert_str_to_set(data[index])        
        index += 1
        
    return str(intersected_letter)


'''def dealing_with_twos(data: list[str], confirmed_letter: str) -> str:
    index = 0
    intersected_letter = convert_str_to_set(data[0])
    while index < len(data) and len(intersected_letter) > 2:
        if confirmed_letter in data[index]:
            intersected_letter &= convert_str_to_set(data[index])        
        index += 1
    return intersected_letter'''

        
        
        

        

print(classify(valid_words_data_arrange("5letters.txt")))
