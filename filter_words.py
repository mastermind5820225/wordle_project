NL = '\n'


def filter_words(data: list[str], guess: str) -> list[str]:

  filtered_words = []
  for word in data:
    if all(char not in word for char in guess):
      filtered_words.append(word)
  return filtered_words


def load_data():
  with open("5letters.txt", 'r') as file:
    words = [line.strip() for line in file]
  return words

data = load_data()
filtered_words = filter_words(data, guess)
print(filtered_words)
