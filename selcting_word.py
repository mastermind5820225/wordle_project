
import random

def read_file(data: str) -> list:
    with open(data, 'r') as file:
        return file.read().strip().split()

def eliminate_letters(word, feedback):
    if feedback == 0:
        return [ch for ch in word]
    return []

def filter_words(data : list[str], guess : str) -> list[str]: 
    filtered_words = []
    for word in data:
        if all(char not in word for char in guess):
            filtered_words.append(word)
    return filtered_words

def load_data():
    with open("5letters.txt", 'r') as file :
        for line in file :
            words = line.strip().split(NL)
            for data in words:
                return data
def get_feedback(guess, target):
    feedback = []
    for g, t in zip(guess, target):
        if g == t:
            feedback.append(2)
        elif g in target:
            feedback.append(1)
        else:
            feedback.append(0)
    return feedback

def check_win(guess, target):
    return guess == target

def play_mastermind(file_name: str):
    words = read_file(file_name)
    target = random.choice(words)
    
    while True:
        guess = input("Enter your guess: ").strip().lower()
        
        if guess not in words:
            return -1
        
        feedback = get_feedback(guess, target)
        print("Feedback:", feedback)
        
        if check_win(guess, target):
            print("WIN!")
            break





